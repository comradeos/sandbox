package main

import "fmt"

func Sum(a, b int) int {
	return a + b
}

func main() {
	fmt.Println(Sum(2, 2))
	fmt.Println(Sum(222, 2))
	fmt.Println(Sum(-2, 3))
	fmt.Println(Sum(-2, -9))
}