package main

import (
    "fmt"
    "log"
    "os"
    "os/exec"
    "syscall"
    "time"
)

// MacOS: brew install ffmpeg

func grabVideo(sec int) {
    fmt.Println("Grabbing video for", sec, "seconds")

    // Создаем файл для записи видео
    outputFile, err := os.Create("/Users/comradeos/Workspace/gitlab/sandbox/golang/tests/002_webcam/output.webm")
    if err != nil {
        log.Fatalf("Failed to create output file: %v", err)
    }
    defer outputFile.Close()

    // Запускаем команду ffmpeg
    cmd := exec.Command("ffmpeg", "-re",
        "-y", // Добавляем флаг для автоматического подтверждения перезаписи
        "-f", "avfoundation",
        "-framerate", "30",
        "-i", "0",
        "-vf", "scale=640:480",
        "-c:v", "libvpx", // Используем кодек VP8 для видео
        "-b:v", "1M", // Устанавливаем битрейт видео
        "-c:a", "libvorbis", // Используем кодек Vorbis для аудио
        "-f", "webm", outputFile.Name())

    // Перенаправляем stderr команды в лог
    cmd.Stderr = os.Stderr

    // Запускаем команду
    if err := cmd.Start(); err != nil {
        log.Fatalf("Failed to start ffmpeg: %v", err)
    }

    // Ждем указанное количество секунд
    time.Sleep(time.Duration(sec) * time.Second)

    // Отправляем сигнал завершения процессу ffmpeg
    if err := cmd.Process.Signal(syscall.SIGTERM); err != nil {
        log.Fatalf("Failed to send SIGTERM to ffmpeg process: %v", err)
    }

    // Ждем завершения процесса
    if err := cmd.Wait(); err != nil {
        log.Fatalf("Failed to wait for ffmpeg process to finish: %v", err)
    }

    fmt.Println("Video recording completed")
}

func main() {
    // Записываем видео в течение 10 секунд
    grabVideo(10)
}