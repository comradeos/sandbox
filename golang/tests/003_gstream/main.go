package main

import (
    "io"
    "log"
    "net"
    "net/http"
)

func streamHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "multipart/x-mixed-replace; boundary=frame")

    conn, err := net.Dial("tcp", "localhost:8081")
    if err != nil {
        http.Error(w, "Unable to connect to video feed", http.StatusInternalServerError)
        return
    }
    defer conn.Close()

    io.Copy(w, conn)
}

func main() {
    http.HandleFunc("/stream", streamHandler)
    log.Println("Starting server on :8080")
    log.Fatal(http.ListenAndServe(":8080", nil))
}

// MacOS

// brew install gstreamer gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gst-libav
// gst-launch-1.0 avfvideosrc ! video/x-raw,framerate=30/1,width=640,height=480 ! videoconvert ! jpegenc ! multipartmux boundary=frame ! tcpserversink host=127.0.0.1 port=8081

// Debian

// sudo apt update
// sudo apt install gstreamer1.0-tools gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav

// gst-launch-1.0 v4l2src ! video/x-raw,framerate=30/1,width=640,height=480 ! videoconvert ! jpegenc ! multipartmux boundary=frame ! tcpserversink host=127.0.0.1 port=8081