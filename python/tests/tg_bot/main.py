import re
import logging
from telegram import Update
from telegram.ext import Application, MessageHandler, filters, ContextTypes

# Включаем логирование
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

# Ваш токен, полученный от BotFather
TOKEN = '7447477377:AAGXpaUY4vbZaelgqF9ssr0sWy2MC_2dsQ8'

async def handle_message(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message_text = update.message.text
    chat_id = update.message.chat_id
    chat_title = update.message.chat.title if update.message.chat.title else "Личная переписка"
    
    logger.info(
        f"Получено сообщение из чата '{chat_title}' (ID: {chat_id}): {message_text}"
    )
    
    if re.search(r'\d', message_text):
        print(f"Сообщение, содержащее цифры: {message_text}")

def main():
    # Создаем Application и передаем ему токен вашего бота
    application = Application.builder().token(TOKEN).build()

    # Регистрируем обработчик сообщений
    message_handler = MessageHandler(
        filters.TEXT & ~filters.COMMAND, handle_message
    )
    application.add_handler(message_handler)

    # Запускаем бота
    application.run_polling()

if __name__ == "__main__":
    main()