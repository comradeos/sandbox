
#### Создать среду
```
python3 -m venv venv
```

#### Активировать
```
source venv/bin/activate
```

#### Деактивировать
```
deactivate
```

#### Установить зависимости
```
pip install -r requirements.txt
```
